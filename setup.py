#!/usr/bin/env python
# -*- coding: utf-8 -*-
import io
import re

from setuptools import setup, find_packages

with io.open("README.md", "rt", encoding="utf8") as f:
    readme = f.read()

with io.open("daiquiri_id00/__init__.py", "rt", encoding="utf8") as f:
    version = re.search(r"__version__ = \"(.*?)\"", f.read()).group(1)

setup(
    name="daiquiri_id00",
    version=version,
    license="GPLv3",
    author="",
    author_email="",
    maintainer="",
    maintainer_email="",
    description="",
    long_description=readme,
    packages=find_packages(exclude=("tests",)),
    include_package_data=True,
    zip_safe=False,
    platforms="Linux",
    python_requires=">= 3.6",
    install_requires=[],
    extras_require={},
    classifiers=[
        "Programming Language :: Python :: 3.8",
    ],
    entry_points={
        "console_scripts": [
            "daiquiri-server-id00 = daiquiri_id00.app:main",
        ]
    },
)
