# daiquiri_id00

An example resource / implementors package for daiquiri

## Installation

Mount the directory into the docker container and set the resources and implementors env variables:

```bash
docker run \
    -v "$(pwd)/daiquiri_id00.git:/daiquiri_id00" \
    -e DAIQUIRI_IMPLEMENTORS=daiquiri_id00.implementors \
    -e DAIQUIRI_RESOURCE_FOLDER=/daiquiri_id00/daiquiri_id00/resources \
    # if not using --link
    -e DAIQUIRI_META_URL=host.docker.internal:3306/test \ 
    ... \
    esrfbcu/daiquiri
```

To install the package connect to the container:

```bash
daiquiri-docker.git/connect.sh
(daiquiri)$ cd /daiquri_id00
(daiquiri)$ pip install -e .
```

Then restart the daiquiri process from supervisor on http://localhost:9032

You can also mount `/daiquiri` and `/daiquiri-ui` to overwrite the relevant source code at runtime
