#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse

from daiquiri.app import run_server


def main():
    """Runs REST server with CLI configuration"""
    root = os.path.dirname(__file__)
    resource_folder = os.path.join(root, "resources")
    if not os.path.isdir(hardware_folder):
        hardware_folder = ""

    parser = argparse.ArgumentParser(description="Daiquri REST server")
    parser.add_argument(
        "--static-folder",
        dest="static_folder",
        default="static.default",
        help="Web server static folder (static.* refers to server resource)",
    )
    parser.add_argument(
        "-p", "--port", dest="port", default=8080, help="Web server port", type=int
    )
    args = parser.parse_args()
    run_server(
        resource_folders=[resource_folder],
        implementors="daiquiri_id00.implementors",
        static_folder=args.static_folder,
        port=args.port,
        static_url_path="/",
    )


if __name__ == "__main__":
    main()
